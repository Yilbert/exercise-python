def area(base,altura):
   area=base*altura/2
   return area


def principal():
    correcto=False
    base=0
    altura=0

    while (not correcto):
          try:
                base=float(input("Por Favor ingresar la Base del triangulo  "))
                altura=float(input("Por favor Ingresar la altura del triangulo  "))
                while base < 0 or altura < 0:
                    print("¡Ha escrito un valores  negativo! Inténtelo de nuevo")
                    base = float(input("Por Favor ingresar la Base del triangulo  "))
                    altura = float(input("Por favor Ingresar la altura del triangulo  "))
                correcto=True
          except ValueError:
              print("Debe de ingresar valores Numericos")
    return print("El area del triangulo es ",area(base,altura))


if __name__ == '__main__':
   principal()