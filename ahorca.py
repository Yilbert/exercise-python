import random
import os

# Grafico del ahorcado
grafico = \
 '''
      ____
     |    N
     0    N
    123   N
   64 57  N    
~~~~~~~~~~N   
'''

doll = 'O/|\/\__'   # arrays de simbolos del muñeco



# Inicializar el Juego
def inicializar_juego(Objeto):
    palabra = random.choice(Objeto).lower()
    tablero = ['_'] * len(palabra)
    return tablero, palabra, []



def mostrar_grafico(Acum_error):
    grafic = grafico
    for i in range(0, len(doll)):
        simbolo = doll[i] if i < Acum_error else ' '
        grafic = grafic.replace(str(i), simbolo)  # al cometer un error , Se reemplaza los numeros del grafico por los simbolos
    print(grafic)



def mostrar_tablero(tablero, letras_erroneas):
    for casilla in tablero:
        print(casilla, end=' ')
    print()
    print()
    if len(letras_erroneas) > 0:
        print('Letras erróneas:', *letras_erroneas)
        print()


# pedir una letra
def pedir_letra(tablero, letras_erroneas):
    valida = False
    while not valida:
        letra = input('Ingresa una letra (a-z): ').lower()
        valida = 'a' <= letra <= 'z' and len(letra) == 1
        if not valida:
            print('Error, POR FAVOR NO INGRESAR NUMEROS NI CARACTERES')
        else:
            valida = letra not in tablero + letras_erroneas
            if not valida:
                print('LETRA REPETIDA, intenta con otra !!!.')

    return letra



def resultado_letra(letra, palabra, tablero, letras_errada):

    if letra in palabra:
        print('Felicitaciones!!! Has acertado una letra.')
        actualizar_tablero(letra, palabra, tablero)

    else:
        print('¡Oh! Letra Incorrecta .')
        letras_errada.append(letra)
        intento=int(letras_errada.index(letra)+1)
        print("LLevas ",intento," Intento fallidos de 8")




def actualizar_tablero(letra, palabra, tablero):
    for indice, letra_palabra in enumerate(palabra):
        if letra == letra_palabra:
            tablero[indice] = letra



def verificar_palabra(tablero):
    return '_' not in tablero


# bucle principal de juego
def jugar_al_ahorcado(palabras):

    tablero, palabra, letras_erroneas = inicializar_juego(palabras)
    while len(letras_erroneas) < len(doll):
        mostrar_grafico(len(letras_erroneas))
        mostrar_tablero(tablero, letras_erroneas)
        letra = pedir_letra(tablero, letras_erroneas)
        resultado_letra(letra, palabra, tablero, letras_erroneas)
        if verificar_palabra(tablero):
            print('¡Enhorabuena, lo has logrado!')
            break
    else:
        print(f'¡Lo siento! ¡Has perdido! La palabra a adivinar era {palabra}.')
        mostrar_grafico(len(letras_erroneas))  # paso 7




def jugar_otra():
    return input('Deseas jugar otra vez (introduce s para sí o cualquier otra cosa para no): ')





if __name__ == '__main__':

    palabras = ["Carnicero", "playa", "cirujano", "tigre",
                "edificio", "cafeteria", "televisor", "urbanizacion",
                "peligro", "princesa", "oficina", "portatil",
                "carretera", "futbol", "teclado", "motocicleta"]


    while True:

        jugar_al_ahorcado(palabras)

        if jugar_otra() != 's': break
