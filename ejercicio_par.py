print("*  Programa que  determina si el numero ingresado es par e impar*")

number_int = int(input("Por favor ingrese un numero entero: "))

if number_int % 2 == 0:
    print("El numero ",number_int ,"es un numero Par.")
elif number_int % 2 == 1:
    print("El numero ", number_int, "es un numero Impar.")

