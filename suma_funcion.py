

def pedirNumeroEntero():
    correcto = False
    num = 0
    while (not correcto):
        try:
            num = int(input("Ingresar un numero\n"))
            correcto = True
        except ValueError:
            print('Error, introduce un numero entero\n')

    return num


def num_suma():
    numero = pedirNumeroEntero()
    while numero < 0:
        print("¡Ha escrito un valores  negativo! Inténtelo de nuevo")
        numero = float(input("Por Favor ingresar un numero  \n"))

    sum = numero*(numero+1)/2
    return sum


if __name__ == '__main__':
    print(num_suma())
